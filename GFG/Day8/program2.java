// Element with right side greater and left side smaller

import java.util.*;

class Compute {
    public int findElement(int arr[], int n){
	    int leftMin[] = new int[n];
	    int rightMax[] = new int[n];

	    Arrays.fill(leftMin,-1);
	    Arrays.fill(rightMax,-1);

	    leftMin[0] = arr[0];
	    for(int i = 1 ; i<n ; i++) {
		    if(leftMin[i-1]<arr[i])
			    leftMin[i] = leftMin[i-1];
		    else
			    leftMin[i] = arr[i];
	   }

	   rightMax[n-1] = arr[n-1];
	   for(int i = n-2 ; i>=0 ; i--) {
		   if(rightMax[i+1]>arr[i]) {
			   rightMax[i] = rightMax[i+1];
		   }else {
			   rightMax[i] = arr[i];
		   }
	   }

	   for(int i = 1 ; i<n-1 ; i++) {
		   if(leftMin[i-1]<arr[i] && rightMax[i+1]>arr[i]) {
			   return arr[i];
		   }
	   }
	   return -1;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{5,6,4,1,7,12,9,1,4,1,11,5,7,1};

		Compute sol = new Compute();

		System.out.println(sol.findElement(arr,arr.length));
	}
}
