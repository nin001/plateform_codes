// Leetcode - 26
//
// Remove duplicate elements from the sorted array

class Solution {
    public int removeDuplicates(int[] arr) {
	
	if(arr.length == 0)
		return 0;
        int insertIndex = 0;

        for(int i = 1 ; i<arr.length ; i++) {
            if(arr[insertIndex] != arr[i]) {
                insertIndex++;
                arr[insertIndex] = arr[i];
            }
        }

        return insertIndex+1;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{0,0,1,1,1,2,2,3,3,4};

		Solution s = new Solution();

		int ret = s.removeDuplicates(arr);

		for(int i = 0 ; i<ret ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
