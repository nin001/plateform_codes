class Solution {
    public static void sort012(int arr[], int n) {
        // code here 
        int i = 0;
        int j = 0;
        int k = n-1;
        int temp = 0;
        
        while(j<=k) {
            if(arr[j] == 0) {
                temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
                i++;
                j++;
            }else if(arr[j] == 1) {
                j++;
            }else if(arr[j] == 2) {
                temp = arr[j];
                arr[j] = arr[k];
                arr[k] = temp;
                k--;
            }
        }
    }
}
