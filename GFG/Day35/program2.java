import java.util.*;

class Solution {

    static long merge(long arr[], long start, long mid, long end) {

        long s1 = mid - start + 1;
        long s2 = end - mid;

        long arr1[] = new long[(int) s1];
        long arr2[] = new long[(int) s2];

        for (long i = 0; i < s1; i++) {
            arr1[(int) i] = arr[(int) (start + i)];
        }

        for (long i = 0; i < s2; i++) {
            arr2[(int) i] = arr[(int) (mid + 1 + i)];
        }

        long i = 0, j = 0, k = start;
        long swaps = 0;

        while (i < s1 && j < s2) {
            if (arr1[(int) i] <= arr2[(int) j]) {
                arr[(int) k] = arr1[(int) i];
                i++;
            } else {
                arr[(int) k] = arr2[(int) j];
                j++;
                swaps += s1 - i;
            }
            k++;
        }

        while (i < s1) {
            arr[(int) k++] = arr1[(int) i++];
        }

        while (j < s2) {
            arr[(int) k++] = arr2[(int) j++];
        }

        return swaps;

    }

    // using merge sort we can find the number of inversion in the array
    static long mergeSortCount(long arr[], long start, long end) {

        long count = 0;

        if (start < end) {
            long mid = (start + end) / 2;

            count += mergeSortCount(arr, start, mid);

            count += mergeSortCount(arr, mid + 1, end);

            count += merge(arr, start, mid, end);
        }

        return count;
    }

    // arr[]: Input Array
    // N : Size of the Array arr[]
    // Function to count inversions in the array.
    static long inversionCount(long arr[], long N) {

        return mergeSortCount(arr, 0, N - 1);
    }
}


class HelloWorld {
    public static void main(String[] args) {
        
        long arr[] = new long[]{2, 4, 1, 3, 5};
        
        System.out.println(Solution.inversionCount(arr,arr.length));
    }
}
