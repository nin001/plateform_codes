class Solution {
    //Function to return a list of integers denoting spiral traversal of matrix.
    static ArrayList<Integer> spirallyTraverse(int matrix[][], int r, int c) {
        // code here 
        
        ArrayList<Integer> retList = new ArrayList<Integer>();
        
        
        // flag to handel the condition when rowLeft is adding 
        // and it is not its first addition
        boolean rowRightflag = false;

        // flags to print
        boolean rowRight = true;
        boolean rowLeft = false , colUp = false , colDown = false;

        // variables that increments , so that visited rows and cols 
        // could be skipped
        int a = 0 , b = 0 , e = 0 , d = 1;

        // iterator variables
        int i = 0 , j = 0;
        while(retList.size() < r*c) {
            if(rowRight) {
                // print logic
                if(rowRightflag) {
                    j++;
                }
                while(j<c-a) {
                    retList.add(matrix[i][j]);
                    j++;
                }
                a++;
                j--;    // prev condition false kela sathi invalid index jhalela tela resolve kela
                // change the flags 
                rowRight = false;
                colDown = true;
            }else if(colDown) {
                i++;
                while(i<r-b) {
                    retList.add(matrix[i][j]);
                    i++;
                }
                b++;
                i--; // invalid index resolve
                //-------
                colDown = false;
                rowLeft = true;
            }else if(rowLeft){
                
                j--;
                while(j >= 0+e) {
                    retList.add(matrix[i][j]);
                    j--;
                }
                e++;
                j++; // resolve illegal index
                //------
                rowLeft = false;
                colUp = true;
            }else {
                
                i--;
                while(i >= 0+d) {
                    retList.add(matrix[i][j]);
                    i--;
                }
                d++;
                i++;        // invalid index resolve
                // -----
                colUp = false;
                rowRight = true;
                rowRightflag = true;
                
            }
            
        }
        return retList;
    }
}
