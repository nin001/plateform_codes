// convert array in zig zag fashion

import java.util.*;

class Solution {
	public void zigzag(int arr[]) {

		Arrays.sort(arr);

		for(int i = 0 ; i<arr.length-1 ; i++) {
			int temp = arr[i];
			arr[i] = arr[i+1];
			arr[i+1] = temp;

			i++;
		}
	}
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{7,5,6,9,1,3,4};

		Solution s = new Solution();

		s.zigzag(arr);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
