class Solution{
    
    // arr: input array
    // n: size of array
    // Function to find the trapped water between the blocks.
    static long trappingWater(int arr[], int n) { 
        // Your code here
        
        /* First approch that came in my mind was to use nested loops
        BUT the challenge was how to know at what heigth we can store the 
        water(trap)*/
        
        /* RigthMax and leftMax arrays can be used to see if we can store 
        water 
        Logic : If a block have both left side and right side greater element
        that means we can store water above that block
        and amount is dependent on the max value on its left and max value on 
        its right*/
        
        long trappedWater = 0;
        int leftMax[] = new int[n];
        int rightMax[] = new int[n];
        
        leftMax[0] = arr[0];
        
        for(int i = 1 ; i<arr.length ; i++) {
            if(arr[i]>leftMax[i-1])
                leftMax[i] = arr[i];
            else
                leftMax[i] = leftMax[i-1];
        }
        
        rightMax[n-1] = arr[n-1];
        
        for(int i = n-2 ; i>=0 ; i--) {
            if(arr[i]>rightMax[i+1]) 
                rightMax[i] = arr[i];
            else
                rightMax[i] = rightMax[i+1];
        }
        
        for(int i = 1 ; i<n-1 ; i++) {  // water fakt madhe trap hou shakto
            trappedWater += Math.min(leftMax[i],rightMax[i]) - arr[i];
        }
        
        return trappedWater;
    } 
}
