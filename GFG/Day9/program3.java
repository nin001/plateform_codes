// Buildings receiving sunlight

class Solution {

    public static int longest(int arr[],int n) {
        int currentMax = arr[0];
        int count = 1;
        for(int i = 1 ; i<n ; i++) {
            if(arr[i]>currentMax) {
                count++;
                currentMax = arr[i];
            }
        }
        
        return count;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{2, 5, 1, 8, 3};
		System.out.println(Solution.longest(arr,arr.length));
	}
}
