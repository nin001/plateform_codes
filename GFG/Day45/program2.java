class Solution {
    void rotateMatrix(int arr[][], int n) {
        // code here
        // first approch 
        // copy array into other array and then iterate and copy again
        // rotated array
        
        int copyOfArr[][] = new int[n][n];
        
        for(int i = 0 ; i<n ; i++) {
            for(int j = 0 ; j<n ; j++) {
                copyOfArr[i][j] = arr[i][j];
            }
        }
        
        int index1 = n-1;
        int index2 = 0;
        
        for(int i = 0 ; i<n ; i++) {
            for(int j = 0 ; j<n ; j++) {
                if(index1 == -1) {
                    index2++;
                    index1 = n-1;
                }
                arr[index1--][index2] = copyOfArr[i][j];
            }
        }
    }

}
