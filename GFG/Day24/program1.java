import java.util.*;

class Solution {

    ArrayList<Integer> findSubarray(int a[], int n) {
        
        ArrayList<Integer> retList = new ArrayList<Integer>();
        
        int currentSum = a[0];
        int maxSum = a[0];
        
        retList.add(a[0]);
        
        for(int i = 1 ; i<n ; i++) {
            int sum = currentSum + a[i];
            if(sum>a[i]) {
                retList.add(a[i]);
                currentSum = sum;
            }else {
                retList.clear();
                retList.add(a[i]);
                currentSum = a[i];
            }
            
            if(maxSum<currentSum) {
                maxSum = currentSum;
            }
        }
        
        if(retList.size() == 0) {
            retList.add(-1);
        }
        
        return retList;
    }
}

class Client {
	public static void main(String args[]) {
		
		int arr[] = new int[]{-1, 2};

		Solution s = new Solution();

		System.out.println(s.findSubarray(arr,arr.length));
	}
}
