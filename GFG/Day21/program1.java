
class Solution {
    int findMaxDiff(int arr[], int n) {
    	// Your code here

    	int ls[] = new int[n];
    	int rs[] = new int[n];
    	int absMax = Integer.MIN_VALUE;

    	// ls array fill
    	for(int i = 1 ; i<n ; i++) {
    	    for(int j = i-1 ; j>=0 ; j--) {
    	        if(arr[i]>arr[j]) {
    	            ls[i] = arr[j];
    	            break;
    	        }
    	    }
    	}

    	// rs array fill
    	for(int i = n-2 ; i>=0 ; i--) {
    	    for(int j = i+1 ; j<n ; j++) {
    	        if(arr[i]>arr[j]) {
    	            rs[i] = arr[j];
    	            break;
    	        }
    	    }
    	}

    	for(int i = 0 ; i<n ; i++) {
    	    if(absMax< Math.abs(ls[i]-rs[i]))
    	        absMax = Math.abs(ls[i]-rs[i]);
    	}

    	return absMax;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{5, 1, 9, 2, 5, 1, 7};

		Solution s = new Solution();

		System.out.println(s.findMaxDiff(arr,arr.length));
	}
}
