// Product of maximum in first and minimum in second

class Solution{
    
    // Function for finding maximum and value pair
    public static long find_multiplication (int arr[], int brr[], int n, int m) {
        // Complete the Function
        
        long prod = Integer.MIN_VALUE;
        for(int i = 0 ; i<n ; i++) {
            if(prod<arr[i]) {
                prod = arr[i];
            }
        }
        
        int min = Integer.MAX_VALUE;
        for(int i = 0 ; i<m ; i++) {
            if(min>brr[i]) {
                min = brr[i];
            }
        }

	System.out.println(prod + " " + min);
        
        return prod*min;
    }
    
    
}

class Client {
	public static void main(String args[]) {

		int A[] = new int[]{5, 7, 9, 3, 6, 2};
		int B[] = new int[]{1, 2, 6, -1, 0, 9};

		System.out.println(Solution.find_multiplication(A,B,A.length,B.length));
	}
}
