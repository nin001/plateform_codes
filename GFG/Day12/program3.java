// left most and right most index

class pair {
	long first,second;
	pair(long first,long second) {
		this.first = first;
		this.second = second;
	}
}

class Solution {
    
    public pair indexes(long arr[], long x) {
        // Your code goes here
        int start = 0;
        int end = arr.length-1;
        int index = -1;
        while(start<=end) {
            int mid = (end-start)/2 + start;
            if(arr[mid] == x) {
                index = mid;
		break;
	    }
            else if(arr[mid]<x)
                start = mid+1;
            else
                end = mid-1;
        }
        
        if(index == -1) {
            return new pair(-1,-1);
        }else {
            pair p = new pair(index,index);
            
            while(p.first>0) {
                if(arr[(int)p.first-1] != x)
                    break;
                p.first--;
            }
            
            while(p.second<arr.length-1) {
                if(arr[(int)p.second+1] != x)
                    break;
                p.second++;
            }
            return p;
        }
        
        
        
    }
}

class Client {
	public static void main(String args[]) {

		long arr[] = new long[]{1,2,3,4,4,4,4,4,5};
		
		Solution s = new Solution();

		pair p = s.indexes(arr,1);

		System.out.println(p.first + " " + p.second);

	}
}
