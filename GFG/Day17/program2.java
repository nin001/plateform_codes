
// Kth smallest element in the array GFG

// Sorting the array where all the elements are sorted then return the k-1 index here
/*
class Solution{
    public static int kthSmallest(int[] arr, int l, int r, int k) { 
        //Your code here
        
        Arrays.sort(arr);
        
        return arr[k-1];
	
/*
	 * TLE 
	int kthSmallest = Integer.MIN_VALUE;
        
        for(int i = 1 ; i<=k ; i++) {
            int tempMin = Integer.MAX_VALUE;
            
            for(int j = 0 ; j<=r ; j++) {
                if(arr[j]<tempMin && arr[j]>kthSmallest)
                    tempMin = arr[j];
            }
            
            kthSmallest = tempMin;
        }
        
        return kthSmallest;
	
    } 
}*/

class Solution{
    public static int kthSmallest(int[] arr, int l, int r, int k) {
        //Your code here

         if (l <= r) {
            int pivotIndex = partition(arr, l, r);

            if (pivotIndex == k - 1) {
                return arr[pivotIndex];
            } else if (pivotIndex < k - 1) {
                return kthSmallest(arr, pivotIndex + 1, r, k);
            } else {
                return kthSmallest(arr, l, pivotIndex - 1, k);
            }
        }
        return -1;
    }

    private static int partition(int[] arr, int l, int r) {
        int pivot = arr[r];
        int i = l - 1;

        for (int j = l; j < r; j++) {
            if (arr[j] <= pivot) {
                i++;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i + 1];
        arr[i + 1] = arr[r];
        arr[r] = temp;

        return i + 1;
    }
}

class Client {

	public static void main(String args[]) {

		int arr[] = new int[]{7,10,4,3,20,15};

		System.out.println(Solution.kthSmallest(arr,0,5,3));
	}
}


