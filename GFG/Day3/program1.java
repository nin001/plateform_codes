// Majority element
// approch 2
// Find candidate element in array and after that count the candidate elements occurance
// if count>arr.length/2 return count

import java.util.*;
import java.io.*;
import java.lang.*;

class Geeks
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        while(t-- > 0)
        {
            int n =sc.nextInt();
            int arr[] = new int[n];
            
            for(int i = 0; i < n; i++)
             arr[i] = sc.nextInt();
             
           System.out.println(new Solution().majorityElement(arr, n)); 
        }
    }
}
// } Driver Code Ends


//User function Template for Java

class Solution
{
    static int majorityElement(int arr[], int size)
    {
        // your code here
        int count = 1;
        int eleIndex = 0;
        
        for(int i = 1 ; i<arr.length ; i++) {
            if(arr[eleIndex]!=arr[i]) {
                count--;
            }else {
                count++;
            }
            
            if(count == 0) {
                count = 1;
                eleIndex = i;
            }
            
        }
        
        count = 0;
        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[eleIndex] == arr[i]) {
                count++;
            }
        }
        
        if(count > arr.length/2) {
            return arr[eleIndex];
        }
        return -1;
    }
}
