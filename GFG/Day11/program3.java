// Array Subset of another array


import java.util.*;
class Compute {
    public String isSubset( long arr1[], long arr2[], long n, long m) {

        Map<Long,Integer> map = new HashMap<Long,Integer>();

        for(int i = 0 ; i<n ; i++) {
            if(map.containsKey(arr1[i])) {
                map.put(arr1[i],map.get(arr1[i])+1);
            }else {
                map.put(arr1[i],1);
            }
        }

        for(int i = 0 ; i<m ; i++) {
            if(map.containsKey(arr2[i])) {
                if(map.get(arr2[i]) > 0) {
                    map.put(arr2[i],map.get(arr2[i])-1);
                }else {
                    return new String("No");
                }
            }else {
                return new String("No");
            }
        }

        return new String("Yes");

    }
}

class Client {
	public static void main(String args[]) {

		long arr1[] = new long[]{10, 5, 2, 23, 19};
		long arr2[] = new long[]{19,5,3};

		Compute obj = new Compute();

		System.out.println(obj.isSubset(arr1,arr2,arr1.length,arr2.length));
        
    }
}
