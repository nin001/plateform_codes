// Product array puzzle

import java.util.*;

class Solution {
	public static long[] productExceptSelf(int arr[], int n) {
        // code here
        long prod[] = new long[n];

        long product = 1;
        int zeroCount = 0;
        for(int i = 0 ; i<n ; i++) {
            if(arr[i] == 0) {
                zeroCount++;
                continue;
            }
            if(zeroCount > 1) {
                product = 0;
                break;
            }
            if(arr[i]!=0)
                product *= arr[i];
        }

        if(zeroCount>1) {
            return prod;
        }else if(zeroCount == 1){
            for(int i = 0 ; i<n ; i++) {
                if(arr[i] != 0) {
                    prod[i] = 0;
                }else {
                    prod[i] = product;
                }
            }
	    return prod;
        }
        for(int i = 0 ; i<n ; i++) {
            prod[i] = product/arr[i];
        }
        

        return prod;
    }
	
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{12,0};

		Solution s = new Solution();

		long ret[] = s.productExceptSelf(arr,arr.length);

		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}
