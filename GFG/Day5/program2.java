// Equilibrium Point GFG
//
// Given an array A of n non negative numbers. The task is to find the first equilibrium point 
// in an array. Equilibrium point in an array is an index (or position) such that the sum of 
// all elements before that index is same as sum of elements after it.

class Solution {

    
    // a: input array
    // n: size of array
    // Function to find equilibrium point in the array.
    public static int equilibriumPoint(long arr[], int n) {

        // Your code here
        for(int i = 1 ; i<n ; i++) {
            arr[i] = arr[i-1] + arr[i];
        }
        
        for(int i = 0 ; i<n ; i++) {
            if(i == 0) {
                if(arr[n-1]-arr[i] == 0) 
                    return i+1;
            }else if(i == n-1) {
                if(arr[i-1] == 0) {
                    return i+1;
                }
            }else {
                long left = arr[i-1];
                long right = arr[n-1]-arr[i];
                if(left == right) 
                    return i+1;
            }
        }
        return -1;
    }
}
