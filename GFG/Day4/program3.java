// Even and Odd
//
import java.io.*;
import java.util.*;


// } Driver Code Ends
//User function Template for Java

class Solution
{
    ArrayList<Long> arrangeOddAndEven(long a[], int n)
    {
        ArrayList<Long> evenLst = new ArrayList<>();
        ArrayList<Long> oddLst = new ArrayList<>();
        ArrayList<Long> res = new ArrayList<>(n);
        for(int i=0;i<n;++i) {
            if(a[i] % 2 == 0) {
                evenLst.add(a[i]);
            }
            else {
                oddLst.add(a[i]);
            }
        }
        int i = 0, j = 0;
        while(i < evenLst.size() && j < oddLst.size()) {
            res.add(evenLst.get(i));
            ++i;
            res.add(oddLst.get(j));
            ++j;
        }
        while(i < evenLst.size()) {
            res.add(evenLst.get(i));
            ++i;
        }
        while(j < oddLst.size()) {
            res.add(oddLst.get(j));
            ++j;
        }
        return res;
    }
}




//{ Driver Code Starts.

// Driver class
class Array {

    // Driver code
    public static void main(String[] args) throws IOException {
        // Taking input using buffered reader
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int testcases = Integer.parseInt(br.readLine());
        // looping through all testcases
        while (testcases-- > 0) {
            int n = Integer.parseInt(br.readLine());
//            String line = br.readLine();
//            String[] q = line.trim().split("\\s+");
//            int n =Integer.parseInt(q[0]);
//            int k =Integer.parseInt(q[1]);
//            //int y =Integer.parseInt(q[2]);
            String line1 = br.readLine();
            String[] a1 = line1.trim().split("\\s+");
            long a[] = new long[n];
            for (int i = 0; i < n; i++) {
                a[i] = Long.parseLong(a1[i]);
            }
//            String line2 = br.readLine();
//            String[] a2 = line2.trim().split("\\s+");
//            long b[] = new long[m];
//            for (int i = 0; i < m; i++) {
//                b[i] = Long.parseLong(a2[i]);
//            }
            Solution ob = new Solution();
            ArrayList<Long> ans=ob.arrangeOddAndEven(a,n);
            for (int i = 0; i < ans.size(); i++)
            {
                System.out.print(ans.get(i)+" ");
            }
            System.out.println();

        }
    }
}
