//First repeating element (gfg)

/*Given an array arr[] of size n, find the first repeating element. The element should occur more than once and the index of its first occurrence should be the smallest.

Note:- The position you return should be according to 1-based indexing.*/ 

import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

// User function Template for Java


class Solution {
    // Function to return the position of the first repeating element.
    public static int firstRepeated(int[] arr, int n) {
        // Your code here
	
	Map<Integer,Integer> map = new HashMap<>();

	int small = -1;
	for(int i = 0 ; i<n ; i++) {
		if(map.containsKey(arr[i])) {
			if(small == -1) {
				small = map.get(arr[i]);
			}else if(map.get(arr[i])<small) {
				small = map.get(arr[i]);
			}
		}else {
			map.put(arr[i],i);
		}
	}

	//System.out.println(map);
	//System.out.println(map.get(arr[1]));
	//System.out.println(map.containsKey(arr[1]));
	return small+1;
    }
}

class Cleint {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size : ");

		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		Solution sol = new Solution();

		System.out.println(sol.firstRepeated(arr,n));
	}
}
