// key pair
/*Given an array Arr of N positive integers and another number X. Determine whether or not 
there exist two elements in Arr whose sum is exactly X.*/

import java.util.*;

class Solution {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here

        Set<Integer> set = new HashSet<Integer>();

        for(int i = 0 ; i<n ; i++) {
            int val = x-arr[i];
            if(set.contains(val)) {
                return true;
            }
            set.add(arr[i]);
        }

        return false;
    }
}

class Client {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println();
		for(int i = 0 ; i<size ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println();
		int x = sc.nextInt();

		Solution obj = new Solution();

		System.out.println(obj.hasArrayTwoCandidates(arr,arr.length,x));
	}
}
