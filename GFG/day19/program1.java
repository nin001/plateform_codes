class Solution{
    //Function to count subarrays with sum equal to 0.
    public static long findSubarray(long[] arr ,int n) {
        //Your code here
        
        Map<Long,Integer> map = new HashMap<Long,Integer>();
        
        long sum = 0;
        
        for(int i =0 ; i < n ; i++){
            sum = sum + arr[i];
            
            if(!map.containsKey(sum)) {
                map.put(sum,1);
            }else {
                map.put(sum,map.get(sum)+1);
            }
        }
        long ans = 0;
        for(Map.Entry<Long,Integer> entry : map.entrySet()){
            int val = entry.getValue();
            if(entry.getKey() == 0){
                ans = ans + ( ((val)*(val+1)) / 2 );
            }
            else{
                ans = ans + ( ((val-1)*(val)) / 2 );
            }
        }
        return ans;
        
    }
}

