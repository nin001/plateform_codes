//User function Template for Java

class Solution {
    //Function to find the minimum number of platforms required at the
    //railway station such that no train waits.
    static int findPlatform(int arr[], int dep[], int n) {
        // add your code here
        
        /*
        There are given arrival array in departure array . 
        The approch i came up with was , checking each element 
        with its ahead elments and checking for intersections
        and find maximum intersections , which will be the 
        minimum platfroms needed
        */
        /*
        int platformsNeeded = 0;
        
        for(int i = 0 ; i<n ; i++) {
            int intersection = 0;
            for(int j = i+1 ; j<n ; j++) {
                if(arr[j]<=dep[i] || dep[j]<=dep[i]) {
                    intersection++;
                }else {
                    break;
                }
            }
            
            if(platformsNeeded<intersection) 
                platformsNeeded = intersection;
        }
        
        return platformsNeeded+1;
        */
        
        Arrays.sort(arr);
        Arrays.sort(dep);
        
        int needed = 1;
        int current = 1;
        
        int i = 1;
        int j = 0;
        
        while(i<n && j<n) {
            if(arr[i]<=dep[j]) {
                i++;
                current++;
                needed = Math.max(current,needed);
            }else {
                current--;
                j++;
            }
        }
        
        return needed;
    }
    
}
