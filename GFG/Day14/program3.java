class Solution {

    public static int smallestSubWithSum(int arr[], int n, int x) {
        // Your code goes here 
        int minSub = Integer.MAX_VALUE;
        int sum = 0;
        int i = 0;
        int j = 0;
        
        while(i<n && j<n) {
            sum += arr[j];
            if(sum>x) {
                if(minSub>j-i+1) {
                    minSub = j-i+1;
                }
                sum = 0;
                i++;
                j = i;
            }else {
            	j++;
	    }
        }
        
        return (minSub==Integer.MAX_VALUE)?0:minSub;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{1, 4, 45, 6, 0, 19};

		System.out.println(Solution.smallestSubWithSum(arr,arr.length,51));
	}
}
