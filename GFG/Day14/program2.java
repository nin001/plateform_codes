import java.util.*;

class Solve {
    
    
    int[] printUnsorted(int[] arr, int n) {
        // code here
        int copy[] = Arrays.copyOf(arr,n);
        
        Arrays.sort(copy);
        
        int ret[] = new int[2];
        ret[0] = -1;
        ret[1] = -1;
        int flag = 0;
        for(int i = 0 ; i<n ; i++) {
            if(arr[i] != copy[i] && flag == 0) {
                ret[0] = i;
                flag = 1;
            }
            if(ret[0] != -1 && copy[i] != arr[i]) {
                ret[1] = i;
            }
        }
        
        if(ret[0] == -1 && ret[1] == -1) {
            ret[0] = 0;
            ret[1] = 0;
        }
        
        return ret;
    }
}
