class Solution {
    int minDist(int arr[], int n, int x, int y) {
    
     int min_dist = Integer.MAX_VALUE;
        int prev = -1;

        for (int i = 0; i < n; i++) {
            if (arr[i] == x || arr[i] == y) {
                if (prev != -1 && arr[prev] != arr[i]) {
                    min_dist = Math.min(min_dist, i - prev);
                }
                prev = i;
            }
        }

        if (min_dist == Integer.MAX_VALUE) {
            return -1; // Either x or y doesn't exist in the array
        }

        return min_dist;
    }
}
