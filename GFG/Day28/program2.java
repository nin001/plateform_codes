import java.util.Arrays;

class Solve {
    int[] findTwoElement(int arr[], int n) {
        // code here
        
        Arrays.sort(arr);
        int ret[] = new int[2];
        
        int ele = 1;
        for(int j = 0 ; j<n-1 ; j++) {
            if(arr[j] == arr[j+1]) {
                ret[0] = arr[j];
                j++;
            }if(ele != arr[j]) {
                ret[1] = ele;
            }
            ele++;
        }
        
        if(ret[1] == 0) {
            ret[1] = ele;
        }
        
        return ret;
    }
}


class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{2,2};

		Solve s = new Solve();

		int ret[] = s.findTwoElement(arr,arr.length);

		System.out.println(ret[0] + " " + ret[1]);
	}
}
