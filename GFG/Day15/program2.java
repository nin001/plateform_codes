// Bubble sort

class Solution {
    //Function to sort the array using bubble sort algorithm.
	public static void bubbleSort(int arr[], int n) {
        //code here
        for(int i = 0 ; i<n ; i++) {
            boolean flag = false;
            for(int j = 0 ; j<n-i-1 ; j++) {
                if(arr[j]>arr[j+1]) {
                    flag = true;
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }

            if(flag == false)
                return;
        }
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{4, 1, 3, 9, 7};

		Solution.bubbleSort(arr,arr.length);
		
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
