// transpose of matrix

class Solution {
    public void transpose(int n,int arr[][]) {
        
        int index = 1;
        
        for(int i = 0 ; i<n-1 ; i++) {
            int swapIndex = index;
            while(swapIndex < n) {
                int temp = arr[i][swapIndex];
                arr[i][swapIndex] = arr[swapIndex][i];
                arr[swapIndex][i] = temp;
                
                swapIndex++;
            }
            index++;
        }
    }
}
