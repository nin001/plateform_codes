class Solution {
    
    public long countSubArrayProductLessThanK(long arr[], int n, long k) {
        
        long count = 0;
        long product = 1;
        int j = 0;
        // sliding window approch 
        // i is end and j is start 
        // we icrease the window and see the produce is increasing the k is increased
        // then we update the count by possible subarrays in the 
        // window that is i-j+1
        for(int i = 0 ; i < n ; i++) {
            product *= arr[i];
            
            while(product >= k && j <= i) {
                product /= arr[j];
                j++;
            }
            
            count += i - j + 1;
        }
        
        return count;

    }
}
