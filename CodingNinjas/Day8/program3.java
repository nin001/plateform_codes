class Solution {
    void rearrange(int arr[], int n) {
        // code here
        
        ArrayList<Integer> positive = new ArrayList<Integer>();
        ArrayList<Integer> negative = new ArrayList<Integer>();
        
        for(int i = 0 ; i<n ; i++) {
            if(arr[i]>=0) {
                positive.add(arr[i]);
            }else {
                negative.add(arr[i]);
            }
        }
        
        
        int i = 0;
        int j = 0;
        int k = 0;
        while(i<positive.size() && j<negative.size()) {
            arr[k++] = positive.get(i++);
            arr[k++] = negative.get(j++);
        }
        
        while(i<positive.size()) {
            arr[k++] = positive.get(i++);
        }
        
        while(j<negative.size()) {
            arr[k++] = negative.get(j++);
        }
        
    }
}
