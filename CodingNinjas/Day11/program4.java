// For coding ninjas streak

//You are given an array ‘ARR’ of size ‘N’ containing each number between 1 and ‘N’ - 1 at least once. There is a single integer value that is present in the array twice. Your task is to find the duplicate integer value present in the array.
/*
For example:

Consider ARR = [1, 2, 3, 4, 4], the duplicate integer value present in the array is 4. Hence, the 
answer is 4 in this case.*/

import java.util.ArrayList;

class Solution {

	public static int findDuplicate(ArrayList<Integer> arr) {

		//    Write your code here.

		int sum = ((arr.size()-1)*arr.size())/2;

		for(int i = 0 ; i<arr.size() ; i++) {
			sum -= arr.get(i);
		}

		return Math.abs(sum);
	}
}

class Client {
	public static void main(String args[]) {

		ArrayList a = new ArrayList();

		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);
		a.add(4);

		System.out.println(Solution.findDuplicate(a));
	}
}


