import java.util.* ;
import java.io.*; 
import java.util.ArrayList;

class Solution {
	public static ArrayList<Integer> sqsorted(ArrayList<Integer> arr) {
		// Write your code here.
		ArrayList<Integer> retList = new ArrayList<Integer>();
		boolean negFlag = false;
		for(int i = 0 ; i<arr.size() ; i++) {
			if(arr.get(i)<0) {
				negFlag = true;
			}
			retList.add(0);
		}

	
		if(negFlag) {
			int i = 0;
			int j = arr.size()-1;
			int k = retList.size()-1;

			while(i<=j) {
				if(Math.abs(arr.get(i)) > arr.get(j)) {
					int p = arr.get(i);
					retList.set(k,p*p);
					k--;
					i++;
				}else {
					int p = arr.get(j);
					retList.set(k,p*p);
					k--;
					j--;
				}
			}
		}else {
			for(int i = 0 ; i<arr.size() ; i++) {
				int p = arr.get(i);
				retList.set(i,p*p);
			}
		}

		return retList;

	
	}
}
