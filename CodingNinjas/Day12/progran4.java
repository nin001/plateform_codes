// medium level coding ninjas
// move zeros to end

import java.util.ArrayList;

class Solution {
	public static void pushZerosAtEnd(ArrayList<Integer> arr) {
		// Write your code here.
		int index = 0;

		for(int i = 0 ; i<arr.size() ; i++) {
			if(arr.get(i)!=0) {
				arr.set(index,arr.get(i));
				index++;
			}
		}

		while(index<arr.size()) {
			arr.set(index,0);
			index++;
		}
	}
}
