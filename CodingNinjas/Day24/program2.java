import java.util.* ;
import java.io.*; 
import java.util.ArrayList;

public class Solution {

  public static ArrayList < Double > findMedian(ArrayList < Integer > arr, int n, int m) {
    // Write your code here.

    ArrayList<Double> retList = new ArrayList<Double>();

    int i = 0;
    int j = m-1;

    int midPoint = j-1+1;

    if(midPoint%2 != 0) {
      int mid = (j-i+1)/2;
      while(j<n) {
        retList.add(arr.get(mid));

        i++;
        j++;
        mid = (j-1+1)/2;
 
      }
    }else {
      int mid1 = (j-1+1)/2;
      int mid2 = mid1+1;

      while(j<n) {
        double d = (arr.get(mid1)+arr.get(mid2))/2;
        retList.add(d);

        i++;
        j++;
        mid1 = (j-1+1)/2;
        mid2 = mid1+1;
        
      }
  }
}
