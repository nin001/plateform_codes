// Wave array
/ Given sorted array (sort in wave manner) 1>=2<=3>=...

import java.util.*;

class Solution {
	public void sortWave(int arr[] , int n) {
		int i = 0;
		int j = 1;

		while(i<n && j<n) {
			int temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;

			i = j+1;
			j = i+1;
		}
	}
}

class Client {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[]{1,2,3,5,6,7};

		Solution s = new Solution();

		s.sortWave(arr,arr.length);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
