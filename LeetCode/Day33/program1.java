/*
 * Approch 1 : use nested loops and find the subarray divisible by k
 *
 * class Solution {
    public int subarraysDivByK(int[] nums, int k) {
        
        int count = 0;

        for(int i = 0 ; i<nums.length ; i++) {
            int sum = nums[i];
            for(int j = i+1 ; j<nums.length ; j++) {
                if(sum%k == 0) 
                    count++;
                sum += nums[j];
            }
            if(sum%k == 0) 
                count++;
        }	*****Time complexity of this is O of n^2

        return count;
    }
}*/

import java.util.*;

/** IN this approch we use the concept of prefix sum and checking for the remainders freq
 * formula = s1 sum gives x when mod by k and s2 also gives x then s2-s1 (subarray) is divisible by k
 * so we add the freq of x stored in map to the count and also increment the freq*/

/** Wrong ans when negative value is present due to negative remainder , so we can add k in the negative and make
 * it positive and check in the subarray*/

class Solution {
    public int subarraysDivByK(int[] arr, int k) {

        int count = 0;

        Map<Integer,Integer> map = new HashMap<>();
        int sum = 0;
        map.put(sum,1);

        for(int i = 0 ; i<arr.length ; i++) {
            sum += arr[i];
	    int rem = sum%k;
	    if(rem<0) {
		    rem += k;
	    }

            if(map.containsKey(rem)) {
                count += map.get(rem);
                map.put(rem,map.get(rem)+1);
            }else {
                map.put(rem,1);
            }
        }

        return count;
    }
}

class Client {
	public static void main(String args[]) {

		Solution s = new Solution();

		int arr[] = new int[]{-1,2,9};

		System.out.println(s.subarraysDivByK(arr,2));
	}
}
