// kth larges element

import java.util.Arrays;

class Solution {
	/*
	 * Approches : 
	 * Nested Loop with K*N which will give time complexity of O(n2) 
	 * looping ktimes n and finding first max and then finding second max lesser than first max
	 * and so on
	 *
	 * Sorting array with Arrays.sort or implementing quick sort , and then just iterating through last index
	 * and finding the kth largest
	 */
    public int findKthLargest(int[] arr, int k) {
        /* WORKS FOR DISTINCT ELEMENTS ONLY
	    int kthLargest = Integer.MAX_VALUE;

	    for(int i = 1 ; i<=k ; i++) {
		    int tempMax = Integer.MIN_VALUE;
		    for(int j = 0 ; j<arr.length ; j++) {
			    if(arr[j]>tempMax && arr[j]<kthLargest) 
				    tempMax = arr[j];
		    }

		    kthLargest = tempMax;
	    }

	    return kthLargest;*/

	    Arrays.sort(arr);

	    return arr[arr.length-k];
    }
}

class Client {
	public static void main(String args[]) {

		Solution s = new Solution();

		int arr[] = new int[]{3,2,3,1,2,4,5,5,6};

		System.out.println(s.findKthLargest(arr,4));
	}
}


