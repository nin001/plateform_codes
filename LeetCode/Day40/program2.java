class Solution {
    public int[] maxSlidingWindow(int[] arr, int k) {

        // first approch , Is using sliding window
        // Window size is k so we first find the maximum in window 
        // from 0 to k and then we just simply increse the value and compare it
        // with the maximum found and then add the maximum in the array

        // int max = Integer.MIN_VALUE;

        // List<Integer> list = new ArrayList<>();

        // for(int i = 0 ; i<k ; i++) {
        //     if(max<arr[i]) {
        //         max = arr[i];
        //     }
        // }
        // if(arr.length == k) {
        //     int ret[] = new int[1];
        //     ret[0] = max;
        //     return ret;
        // }

        // list.add(max);
        // int j = k;
        // while(j<arr.length) {
        //     if(arr[j]>max) {
        //         max = arr[j];
        //         list.add(arr[j]);
        //     }else {
        //         list.add(max);
        //     }
        //     j++;
        // }

        // int ret[] = new int[list.size()];

        // for(int i = 0 ; i<list.size() ; i++) {
        //     ret[i] = list.get(i);
        // }

        // return ret;

        // This approch was incorrect as i was just comparing the last element 
        // asuming the max would always be at the j index

        //Second approch is using SortedSet which will sort the array
        // We can access the maximum by set.last() method and retrieve max of window
        // shifting window i++,j++ and removing the previous element from set and 
        // adding element and incrementing j where i and j is the window pointers
        
        /*
        [-7,-8,7,5,7,1,6,0]
        k = 4
        output ; [7,7,7,6,6]
        expected : [7,7,7,7,7]
        This test case was failed , because of set Class which does not allow duplicate elements: 
TO get rid of this problem we can use List interface and use Collections.sort 
        TreeSet<Integer> set = new TreeSet<Integer>();
        */

        /*
        List<Integer> list = new ArrayList<>();

        int ret[] = new int[arr.length-k+1];
        int index = 0;

        for(int i = 0 ; i<k ; i++) {
            list.add(arr[i]);
        }

        Collections.sort(list);
        ret[index++] = list.get(k-1);

        int i = 1;
        int j = k;

        while(j<arr.length && index<ret.length) {
            list.remove(list.indexOf(arr[i-1]));
            list.add(arr[j]);

            Collections.sort(list);

            ret[index++] = list.get(k-1);
            i++;
            j++;
        }
        
        return ret;

        Solution was giving correct output but TLE at test Case 37
        */

        Deque<Integer> qi = new LinkedList<Integer>();

        int i;
        for (i = 0; i < k; ++i) {
            while (!qi.isEmpty() && arr[i] >= arr[qi.peekLast()])
                qi.removeLast();
            qi.addLast(i);
        }

        int ret[] = new int[arr.length-k+1];
        int index = 0;
        for (; i < arr.length; ++i) {
            ret[index++] = arr[qi.peek()];

            while ((!qi.isEmpty()) && qi.peek() <= i - k)
                qi.removeFirst();

            while ((!qi.isEmpty()) && arr[i] >= arr[qi.peekLast()])
                qi.removeLast();

            qi.addLast(i);
        }

        ret[index++] = arr[qi.peek()];

        return ret;
    }
}
