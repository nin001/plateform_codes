class Solution {
    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        
        final int MOD = 1000000007;
        
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        
        long maxHorizontalGap = Math.max(horizontalCuts[0], h - horizontalCuts[horizontalCuts.length - 1]);
        long maxVerticalGap = Math.max(verticalCuts[0], w - verticalCuts[verticalCuts.length - 1]);
        
        for (int i = 1; i < horizontalCuts.length; i++) {
            maxHorizontalGap = Math.max(maxHorizontalGap, horizontalCuts[i] - horizontalCuts[i - 1]);
        }
        
        for (int i = 1; i < verticalCuts.length; i++) {
            maxVerticalGap = Math.max(maxVerticalGap, verticalCuts[i] - verticalCuts[i - 1]);
        }
        
        long maxArea = (maxHorizontalGap * maxVerticalGap) % MOD;
        return (int) maxArea;
    }
}
