class RandomizedSet {

    Map<Integer,Integer> map;
    List<Integer> al;
    Random random;

    // Taken 2 things map to store element with its index mapped and a arraylist to 
    // store elements 
    // reason to use arraylist so that it gives indexes (List - index Access)
    // and reason to use map is to map the obtained indexed so that search time is minimized

    public RandomizedSet() {
        map = new HashMap<Integer,Integer>();       // initialisation of instance varibalees
        al = new ArrayList<Integer>();
        random = new Random();

    }
    
    public boolean insert(int val) {
        if(map.containsKey(val)) {  // if element is already present that means no insertion can happen return false
            return false;
        }

        al.add(val);    // add element 
        map.put(val,al.size()-1);   // as only 1 element is present And element is adding in last(Insertion order is preserved in List)
                                // we add element and put index size() - 1 
        return true;    // element inserted so return true
    }
    
    public boolean remove(int val) {
        if(!map.containsKey(val)) {     // element is not present so cant remove return false
            return false;
        }

        int index = map.get(val);   // as element is present we just take the index of that element from map
        int elementAtLast = al.get(al.size()-1);    // and take the element at last index in List
        al.set(index,elementAtLast);  // put the last element at the elements index we retrieved so that last element can be removed
        map.put(elementAtLast,index); // update the map(index obtained of last element)
        al.remove(al.size()-1); // remove the last element from list
        map.remove(val);    // remove the element Who is mapped (val) which we desired to remove
        return true;
    }
    
    public int getRandom() {
        int randomIndex = random.nextInt(al.size());    // Random class gives int 0-size-1
        return al.get(randomIndex); //return the index (random) elment
    }
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
