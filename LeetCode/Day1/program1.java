// Single Number Leetcode 136
//
// Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

//You must implement a solution with a linear runtime complexity and use only constant extra space.


import java.util.Arrays;
import java.util.Scanner;

class Solution {
    public int singleNumber(int[] nums) {
        Arrays.sort(nums);

        int i = 0;
        int j = i+1;

        while(i<nums.length && j<nums.length) {
            if(nums[i]!=nums[j]) {
                return nums[i];
            }else {
                i = j+1;
                j = i+1;
            }
        }

        return nums[i];
    }
}

class Client {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int s = sc.nextInt();

		int arr[] = new int[s];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<s ; i++) {
			arr[i] = sc.nextInt();
		}

		Solution sol = new Solution();

		System.out.println("Single Number : "+sol.singleNumber(arr));
	}
}
