// Leetcode - 66 | Plus onw
/*
class Solution {
    public int[] plusOne(int[] digits) {
        long no = 0;
        for(int i = 0 ; i<digits.length ; i++) {
            no = no*10 + digits[i];
        }

        long plusOne = no+1;

	System.out.println(plusOne);
        int digCount = 0;
        while(plusOne != 0) {
            plusOne /= 10;
            digCount++;
        }

        int ret[] = new int[digCount];
        plusOne = no+1;
        no = ret.length-1;

	System.out.println(plusOne);
	System.out.println(digCount);
        while(digCount>0) {
            ret[digCount-1] = (int)plusOne%10;
	    System.out.println(ret[digCount-1]);
            plusOne /= 10;
            digCount--;
        }

        return ret;
    }
}*/
class Solution {
    public int[] plusOne(int[] digits) {

        for(int i=digits.length-1; i>=0; i--){
            if(digits[i] != 9){
                digits[i]+=1;
                return digits;
            }else{
                digits[i] = 0;
            }
        }
        int[] ret = new int[digits.length+1];
        ret[0] = 1;

        return ret;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{9,8,7,6,5,4,3,2,1,0};

		Solution s = new Solution();

		int ret[] = s.plusOne(arr);

		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i]);
		}
		System.out.println();
	}
}
