class Solution {
    public int countQuadruplets(int[] nums) {
        int count=0;
        int n=nums.length;
        for(int i=0;i<n-3;i++){
            for(int j=i+1;j<n-2;j++){
                for(int k=j+1;k<n-1;k++){
                    for(int l=k+1;l<n;l++){
                        if(nums[i] + nums[j] + nums[k] == nums[l]){
                            count = count+1;
                        }
                    }
                }
            }
        }
      return count;
    }
}

class Client {
	public static void main(String args[]) {

		Solution s = new Solution();

		int arr[] = new int[]{1,2,3,6};

		System.out.println(s.countQuadruplets(arr));
	}
}
