// Move zeros to end

class Solution {
    public void moveZeroes(int[] arr) {
        int index = 0;
        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i] != 0) {
                arr[index++] = arr[i];
            }
        }

        while(index<arr.length) {
            arr[index++] = 0;
        }
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{0};

		Solution s = new Solution();

		s.moveZeroes(arr);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] +  " ");
		}
		System.out.println();
	}
}
