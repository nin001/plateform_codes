// Check if array is Rotated Sorted
// Leetcode - 1752

class Solution {
    public boolean check(int[] arr) {
        int flag = 0;
	int peekIndex = -1;
        for(int i = 0 ; i<arr.length-1 ; i++) {
		if(arr[i]>arr[i+1]) {
			flag = 1;
			peekIndex = i;
			break;
		}
	}

	if(peekIndex == -1) {
		return true;
	}else {
		int i = peekIndex;
		while(true) {
			if(i == peekIndex+1) 
				break;
			if(i == 0) {
				if(arr[i]<arr[arr.length-1])
					return false;
				else
					i = arr.length-1;
			}
			if(arr[i]<arr[i-1])
				return false;
			i--;
		}
	}

	return true;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{1,2,3};

		Solution s = new Solution();

		System.out.println(s.check(arr));
	}
}
