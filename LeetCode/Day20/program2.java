import java.util.*;

class Solution {
    public int findDuplicate(int[] arr) {
        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        for(int i = 0 ; i<arr.length ; i++) {
            if(map.containsKey(arr[i])) {
                map.put(arr[i],map.get(arr[i])+1);
                if(map.get(arr[i]) > 1) {
                    return arr[i];
                }
            }else {
                map.put(arr[i],1);
            }
        }
        
        return -1;
    }
}
