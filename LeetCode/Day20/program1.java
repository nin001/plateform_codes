import java.util.*;

class Solution {
    public List<Integer> findDuplicates(int[] nums) {

        Map<Integer,Integer> map = new HashMap<Integer,Integer>();

        for(int i = 0 ; i<nums.length ; i++) {
            if(map.containsKey(nums[i])) {
                map.put(nums[i],map.get(nums[i])+1);
            }else {
                map.put(nums[i],1);
            }
        }

        List<Integer> retList = new ArrayList<Integer>();

        for(int i = 0 ; i<nums.length ; i++) {
            if(map.get(nums[i])>1) {
                if(!retList.contains(nums[i])) {
                    retList.add(nums[i]);
                }
            }
        }

        return retList;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{1};
		
		Solution s = new Solution();

		System.out.println(s.findDuplicates(arr));	
	}
}
