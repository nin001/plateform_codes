// Subarray sum equals k

class Solution {
    public int subarraySum(int[] arr, int k) {
	int count = 0;
        int i = 0;
        int j = 0;
        int sum = 0;

        while(i<arr.length && j<arr.length ) {
            sum += arr[j];

            if(sum == k) {
		    count++;
		    sum -= arr[i];
		    i++;
		    j++;
	    }
            else if(sum>k) {
                sum -= arr[i];
                i++;
            }else {
                j++;
            }
        
	}

	if(sum == k)
		count++;

        return count;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{1,2,3};

		Solution s = new Solution();

		System.out.println(s.subarraySum(arr,3));
	}
}
