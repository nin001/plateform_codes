class Solution {
    public int maxScore(int[] cardPoints, int k) {
        /*
        // Approch was to use two pointers , 1 pointing to start of the row and 2 to end 
        // of the row
        // comparing the cards present at pointers
        // Key logic : if same power present at the start and end , we compare start+1 and end-1 
        // which ever is max we choose that side and increment/decrement the value 
        // which will maximize the points

        int start = 0;
        int end = cardPoints.length-1;
        int maxPoints = 0;

        while(start<=end && k>0) {
            
            if(cardPoints[start]<cardPoints[end]) {
                maxPoints += cardPoints[end];
                end--;
            }else if(cardPoints[start]>cardPoints[end]) {
                maxPoints += cardPoints[start];
                start++;
            }else {
                if(end-start >= 1) {
                    if(cardPoints[start+1]>cardPoints[end-1]) {
                        maxPoints += cardPoints[start];
                        start++;
                    }else {
                        maxPoints += cardPoints[end];
                        end--;
                    }
                }else {
                    maxPoints += cardPoints[start];
                    start++;
                }
            }
            k--;
        }
        return maxPoints;

        Fails for test case where , start and end , end has high value but later start+1 or i has high value 
        which will get eliminated
        */

        /**Sliding window approch */
        int n = cardPoints.length;
        
        // Calculate the total sum of card points
        int totalSum = 0;
        for (int point : cardPoints) {
            totalSum += point;
        }
        
        // Calculate the initial sum of the last k elements
        int windowSum = 0;
        for (int i = n - k; i < n; i++) {
            windowSum += cardPoints[i];
        }
        
        // Initialize the maximum score with the sum of the last k elements
        int maxScore = windowSum;
        
        // Slide the window to the left and update the maximum score
        for (int i = 0; i < k; i++) {
            windowSum -= cardPoints[n - k + i];
            windowSum += cardPoints[i];
            maxScore = Math.max(maxScore, windowSum);
        }
        
        // Return the maximum score
        return maxScore;
        
    }
}
