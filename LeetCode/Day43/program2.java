class Solution {
    public List<String> letterCombinations(String digits) {

        Map<Character,Set<Character>> map = new HashMap<>();

        map.put('2',new HashSet(Arrays.asList('a','b','c')));
        map.put('3',new HashSet(Arrays.asList('d','e','f')));
        map.put('4',new HashSet(Arrays.asList('g','h','i')));
        map.put('5',new HashSet(Arrays.asList('j','k','l')));
        map.put('6',new HashSet(Arrays.asList('m','n','o')));
        map.put('7',new HashSet(Arrays.asList('p','q','r','s')));
        map.put('8',new HashSet(Arrays.asList('t','u','v')));
        map.put('9',new HashSet(Arrays.asList('w','x','y','z')));

        int len = digits.length();

        List<String> retList = new ArrayList<String>();

        if(len == 0) {
            return retList;
        }else if(len == 2) {
            Set<Character> cSet1 = map.get(digits.charAt(0));
            Set<Character> cSet2 = map.get(digits.charAt(1));

            for(char ch1 : cSet1) {
                for(char ch2 : cSet2) {
                    retList.add(new String(new char[]{ch1,ch2}));
                }
            }
        }else if(len == 3) {
            Set<Character> cSet1 = map.get(digits.charAt(0));
            Set<Character> cSet2 = map.get(digits.charAt(1));
            Set<Character> cSet3 = map.get(digits.charAt(2));

            for(char ch1 : cSet1) {
                for(char ch2 : cSet2) {
                    for(char ch3 : cSet3) {
                        retList.add(new String(new char[]{ch1,ch2,ch3}));
                    }
                }
            }

        }else if(len == 4) {
            Set<Character> cSet1 = map.get(digits.charAt(0));
            Set<Character> cSet2 = map.get(digits.charAt(1));
            Set<Character> cSet3 = map.get(digits.charAt(2));
            Set<Character> cSet4 = map.get(digits.charAt(3));

            for (char ch1 : cSet1) {
                for (char ch2 : cSet2) {
                    for (char ch3 : cSet3) {
                        for (char ch4 : cSet4) {
                            retList.add(new String(new char[]{ch1, ch2, ch3, ch4}));
                        }
                    }
                }
            }

            

        }else {
            Set<Character> cSet = map.get(digits.charAt(0));

            for(char ch : cSet) {
                retList.add(new String(new char[]{ch}));
            }
        }

        return retList;
    }
}
