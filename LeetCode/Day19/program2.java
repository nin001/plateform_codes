class Solution {
    public int[] productExceptSelf(int[] arr) {
        int n = arr.length;
        int prod[] = new int[n];
        
        int product = 1;
        int zeroCount = 0;
        for(int i = 0 ; i<n ; i++) {
            if(arr[i] == 0) {
                zeroCount++;
                continue;
            }
            if(zeroCount > 1) {
                product = 0;
                break;
            }
            if(arr[i]!=0)
                product *= arr[i];
        }
        
        if(zeroCount>1) {
            return prod;
        }else if(zeroCount == 1){
            for(int i = 0 ; i<n ; i++) {
                if(arr[i] != 0) {
                    prod[i] = 0;
                }else {
                    prod[i] = product;
                }
            }
            return prod;
        }
        for(int i = 0 ; i<n ; i++) {
            prod[i] = product/arr[i];
        }
        
        
        return prod;
    }
}
