// Number of zeros filled subarrays
// approch - Naive - Nested loops for finding subarrays and keeping count of
// subarray with only 0 as element
//
// - Sliding window approch
// keep a window and when found 0 increase count and if not found 0 slide window

/*
class Solution {
    public long zeroFilledSubarray(int[] arr) {
        long count = 0;

        int i = 0;
        int j = 0;

        while(j<arr.length) {
            if(arr[j] != 0) {
                i++;
                j = i;
            }else {
                count++;
                j++;
            }
        }

        return count;
    }
}

This code doesnt work when 0s are at the end
*/
/*
 * This code fails for larger values
class Solution {
    public long zeroFilledSubarray(int[] arr) {
        long count = 0;

        int start = -1;
        int end = -1;

        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i] == 0 && start == -1) {
                start = i;
            }
            if(arr[i] == 0 && start != -1) {
                end = i;
            }

            if(arr[i] != 0 && start != -1 && end != -1) {
                int n = end-start+1;
                count += (n*(n+1))/2;
                start = -1;
                end = -1;
            }
        }

        if(start != -1 && end != -1) {
            int n = end-start+1;
            count += (n*(n+1))/2;
        }

        return count;
    }
}
*/

// follows concept of guass law , n(n+1)/2 -> which is also the sum of possible subarrays

class Solution {
    public long zeroFilledSubarray(int[] arr) {
        long count = 0;

        long sum = 0;

        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i] == 0) {
                count++;
            }else {
                count = 0;
            }
            sum = sum+count;
        }

        return sum;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{};
		Solution s = new Solution();

		System.out.println(s.zeroFilledSubarray(arr));
	}
}
