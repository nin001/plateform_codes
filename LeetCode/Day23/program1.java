// Game of lives

class Solution {
    private int liveCount(int arr[][] , int i , int j) {
        if(i<0 || j<0 || i>=arr.length || i>=arr[0].length || j>=arr.length || j>=arr[0].length)
            return 0;
        return arr[i][j];
    }

    public void gameOfLife(int[][] board) {

        for(int i = 0 ; i<board.length ; i++) {
            for(int j = 0 ; j<board[0].length ; j++) {
                int nLivesCount = liveCount(board,i-1,j-1)+
                                  liveCount(board,i-1,j)+
                                  liveCount(board,i-1,j+1)+
                                  liveCount(board,i,j-1)+
                                  liveCount(board,i,j+1)+
                                  liveCount(board,i+1,j-1)+
                                  liveCount(board,i+1,j)+
                                  liveCount(board,i+1,j+1);
                if(board[i][j] == 0) {
                    if(nLivesCount == 3) {
                        board[i][j] = 1;
                    }
                }else {
                    if(nLivesCount<2 || nLivesCount>3) {
                        board[i][j] = 0;
                    }
                }
            }
        }
    }
}

class Client {
	public static void main(String args[]) {

		int arr[][] = new int[][]{ {0, 1, 0},
    {0, 0, 1},
    {1, 1, 1},
    {0, 0, 0}};

		Solution s = new Solution();

		s.gameOfLife(arr);

		for(int i = 0 ; i<arr.length ; i++) {
			for(int j = 0 ; j<arr[0].length ; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
}
