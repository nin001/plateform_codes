class Solution {
    public boolean canJump(int[] arr) {
        
        /**** During this problem solving i came with an approch 
        similar to 2 pointer approch , start iterating from the start
        and increment the second pointer to the arr[i] jumps and if found 
        0 in any scenario , just increment the start pointer

        */
        
        // if(arr.length == 1 && arr[0]>=0) return true; 

        // int i = 0;
        // int j = 0;

        // while(i<arr.length) {
        //     if(arr[i] == 0) {
        //         i++;
        //         j = i;
        //     }

        //     if(arr[j] == 0) {
        //         i++;
        //         j = i;
        //     }else {
        //         j += arr[j];
        //         if(j>=arr.length-1)
        //             return true;
        //     }
        // }

        // return false;

        if (arr.length == 1 && arr[0] >= 0)
            return true;

        int furthest = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i > furthest) // If the current index is unreachable
                return false;
            
            furthest = Math.max(furthest, i + arr[i]); // Update the furthest reachable index
            if (furthest >= arr.length - 1) // If we can reach the last index
                return true;
        }

        return false;
        
    }
}
