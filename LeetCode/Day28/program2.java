// find missing and repeating elements

class Solution {
    public int[] findMissingAndRepeatedValues(int[][] grid) {

        int countArr[] = new int[(grid.length * grid[0].length) + 1];
        int ret[] = new int[2];
        for(int i = 0 ; i<grid.length ; i++) {
            for(int j = 0 ; j<grid[0].length ; j++) {
                countArr[grid[i][j]]++;
            }
        }

	int flag1 = 0;
	int flag2 = 1;
        for(int i = 1 ; i<countArr.length ; i++) {
            if(countArr[i] == 0) {
		    flag1 = 1;
		    ret[1] = i;
	    }else if(countArr[i] == 2) {
		    flag2 = 1;
		    ret[0] = i;
	    }

	    if(flag1 == 1 && flag2 == 1) {
		    break;
	    }
        }

        return ret;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[][] = new int[][]{{1,3},{2,2}};

		Solution s = new Solution();

		int ret[] = s.findMissingAndRepeatedValues(arr);

		System.out.println(ret[0] + " " + ret[1]);
	}
}
