
// Majotity element 2 -- leetcode 229

import java.util.*;

class Solution {
    public List<Integer> majorityElement(int[] arr) {

        List<Integer> retList = new ArrayList<Integer>();

        Map<Integer,Integer> map = new HashMap<Integer,Integer>();
        int freq = Math.abs(arr.length/3);
        for(int i = 0 ; i<arr.length ; i++) {
            if(map.containsKey(arr[i])) {
                if(map.get(arr[i])+1 > freq) {
                    if(!retList.contains(arr[i])) {
                        retList.add(arr[i]);
                    }
                }
                map.put(arr[i],map.get(arr[i])+1);
            }else {
		map.put(arr[i],1);
		if(1 > freq) {
                    if(!retList.contains(arr[i])) {
                        retList.add(arr[i]);
                    }
                }
               
            }
        }

        return retList;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{1};

		Solution s = new Solution();

		System.out.println(s.majorityElement(arr));
	}
}
