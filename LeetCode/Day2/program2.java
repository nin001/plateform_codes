// Remove element Leetcode 27

/*Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. The order of the elements may be changed. Then return the number of elements in nums which are not equal to val.

Consider the number of elements in nums which are not equal to val be k, to get accepted, you need to do the following things:

Change the array nums such that the first k elements of nums contain the elements which are not equal to val. The remaining elements of nums are not important as well as the size of nums.
Return k.*/

import java.util.*;

class Solution {
    public int removeElement(int[] nums, int val) {
/* Failed for test cases 
	    int i = 0;
	    int j = nums.length-1;

	    int k = 0;
	    while(i<j) {
		    if(nums[i] == val) {
			    k++;
			    int temp = nums[i];
			    nums[i] = nums[j];
			    nums[j] = temp;
			    j--;
		    }else {
			    i++;
		    }
	    }

	    return nums.length-k;*/

	    int count = 0;
	    for(int i = 0 ; i<nums.length ; i++) {
		    if(nums[i] != val) {
			    nums[count++] = nums[i];
		    }
	    }

	    return count;
    }
}

class Client {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter element : ");
		int val = sc.nextInt();

		Solution s = new Solution();

		System.out.println("K : "+s.removeElement(arr,val));
	}
}
