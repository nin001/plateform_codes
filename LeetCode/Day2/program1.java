//Two Sum Leetcode

/*Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.*/

import java.util.*;

class Solution {
    public int[] twoSum(int[] nums, int target) {
	    int ret[] = new int[2];
	    ret[0] = -1;
	    ret[1] = -1;

	    for(int i = 0 ; i<nums.length ; i++) {
		    for(int j = i+1 ; j<nums.length ; j++) {
			    if(nums[i]+nums[j] == target) {
				    ret[0] = i;
				    ret[1] = j;
				    return ret;
			    }
		    }
	    }
	    return ret;
    }
}

class Client {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<size ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter target :");
		int target = sc.nextInt();

		Solution s = new Solution();

		int ret[] = s.twoSum(arr,target);

		System.out.println(ret[0] + " " + ret[1]);
	}
}
