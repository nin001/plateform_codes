// LeetCode 169 | Majority Element
// Given an array nums of size n, return the majority element.

/*The majority element is the element that appears more than ⌊n / 2⌋ times. 
You may assume that the majority element always exists in the array.*/

import java.util.*;

class Solution {
    public int majorityElement(int[] arr) {

	    Map<Integer,Integer> map = new HashMap<Integer,Integer>();

	    int maxCount = 0;
	    int ele = 0;
	    for(int i = 0 ; i<arr.length ; i++) {
		    if(map.containsKey(arr[i])) {
			    int val = map.get(arr[i]);
			    map.replace(arr[i],val,val+1);
			    if(map.get(arr[i]) >= (arr.length/2)) {
				    if(maxCount<val+1) {
					    maxCount = val+1;
					    ele = arr[i];
				    }
			    }
		    }else {
			    map.put(arr[i],1);
		    }
	    }
	    return ele;
    }
}

class Client {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();
		}

		Solution s = new Solution();

		System.out.println("Majority Element : " + s.majorityElement(arr));
	}
}

