class Solution {
    public int maxArea(int[] height) {

        /**This was my first approch i came up with
        
        In this approch we intend to calculate each and every area
        and then store the max in outside variable
        
        Looping in unidirectional like i = 0 ; i<n ; i++ and nested j = i+1 ; j<n ; j++
        
         */
        /*
        int maxArea = Integer.MIN_VALUE;

        for(int i = 0 ; i<height.length ; i++) {
            int area = 0;
            int width = 1;
            for(int j = i+1 ; j<height.length ; j++) {
                area = Math.min(height[i],height[j])*(width);
                width++;
                if(maxArea<area) {
                    maxArea = area;
                }
            }
        }

        return maxArea;

        Above code has time complexity of n^2 which makes code to
        occur TLE
        */

        /**Other approch of solving this could be of two pointers
        
        placing one to start and other to end 
        calculating the area and updating the maxArea,
        if(startHeight<endHeigth) so we increase the start pointer 
        else we decrease the end pointer
        
         */

        int i = 0;
        int j = height.length-1;
        int maxArea = Integer.MIN_VALUE;

        while(i<j) {
            int area = Math.min(height[i],height[j])*(j-i);

            maxArea = Math.max(maxArea,area);
            if(height[i]<height[j]) {
                i++;
            }else {
                j--;
            }
        }

        return maxArea;
    }
}

