class Solution {
    public int furthestBuilding(int[] heights, int bricks, int ladders) {
        
        // for(int i = 1 ; i<heights.length ; i++) {
        //     if(heights[i-1]>=heights[i]) {
        //         continue;
        //     }else {
        //         if(bricks >= (heights[i]-heights[i-1])) {
        //             bricks -= heights[i]-heights[i-1];
        //         }else if(ladders > 0) {
        //             ladders--;
        //         }else {
        //             return i-1;
        //         }
        //     }
        // }

        // return heights.length - 1;

        // Above approch doesnt solve properly because it simply uses all the bricks first
        // but doesnt calculate which to use for furthest

        PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();

        for(int i = 1 ; i<heights.length ; i++) {
            if(heights[i]-heights[i-1] > 0) {       // if difference in negative
                bricks -= heights[i]-heights[i-1];  // subtract difference from the bricks
                
                minHeap.offer(heights[i-1]-heights[i]);
                if(bricks<0) {  // if bricks becomes negative
                    //check if ladders exist and use ladders and increment the bricks
                    if(ladders>0) {

                        bricks += Math.abs(minHeap.poll());
                        ladders--;  //use ladders so decrement
                    }else {
                        return i-1;     // both ladders as well as  bricks are over
                    }
                }
            }
        }

        return heights.length - 1;
    }
}
