class Solution {
    public int[][] construct2DArray(int[] arr, int m, int n) {
        if(m*n != arr.length) 
            return new int[0][0];
        
        int index = 0;
        int ret[][] = new int[m][n];

        for(int i = 0 ; i<ret.length ; i++) {
            for(int j = 0 ; j<ret[0].length ; j++) {
                ret[i][j] = arr[index];
                index++;
            }
        }

        return ret;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{1,2};

		Solution s  = new Solution();
		int ret[][] = s.construct2DArray(arr,1,1);

		for(int i = 0 ; i<ret.length ; i++) {
			for(int j = 0 ; j<ret[0].length ; j++) {
				System.out.print(ret[i][j]);
			}
			System.out.println();
		}
	}
}
