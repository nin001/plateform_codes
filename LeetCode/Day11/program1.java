// Running sum of array - leetcode 1480

class Solution {
    public int[] runningSum(int[] arr) {
        int runningSum[] = new int[arr.length];

        runningSum[0] = arr[0];
        for(int i = 1 ; i<arr.length ; i++) {
            runningSum[i] = runningSum[i-1] + arr[i];
        }

        return runningSum;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{3,1,2,10,1};

		Solution s = new Solution();

		int ret[] = s.runningSum(arr);
		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}
