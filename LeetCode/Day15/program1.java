// Pascals triangle

import java.util.List;
import java.util.ArrayList;

class Solution {

    public List<List<Integer>> generate(int numRows) {

        List<List<Integer>> retList = new ArrayList<List<Integer>>();

        for(int i = 0 ; i<numRows ; i++) {
            List<Integer> l = new ArrayList<Integer>();
            for(int j = 0 ; j<=i ; j++) {
                if(j == 0) {
                    l.add(1);
                }else if(j == i) {
                    l.add(1);
                }else {
                    List<Integer> temp = retList.get(i-1);
                    l.add(temp.get(j) + temp.get(j-1));
                }
            }
	    
            retList.add(l);
        }

        return retList;
    }
}

class Client {
	public static void main(String args[]) {
		
		Solution s = new Solution();

		List<List<Integer>> ret = s.generate(5);

		for(int i = 0 ; i<ret.size() ; i++) {
			System.out.println(ret.get(i));
		}
	}
}
