// Squares of a sorted array

import java.util.*;

class Solution {
    public int[] sortedSquares(int[] arr) {
	    boolean negFlag = false;
	    for(int i = 0 ; i<arr.length ; i++) {
		    if(arr[i]<0) {
			    negFlag = true;
			    break;
		    }
	    }

	    int ret[] = new int[arr.length];
	    if(negFlag) {
		    int i = 0;
		    int j = arr.length - 1;
		    int k = ret.length - 1;
		    while(i<=j) {
			    if(Math.abs(arr[i])>arr[j]) {
				    ret[k] = arr[i]*arr[i];
				    k--;
				    i++;
			    }else {
				    ret[k] = arr[j]*arr[j];
				    j--;
				    k--;
			    }
		    }
	    }else {
		    for(int i = 0 ; i<arr.length ; i++) {
			    ret[i] = arr[i]*arr[i];
		    }
	    }

	    return ret;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{-7,-3,2,3,11};

		Solution s = new Solution();

		int ret[] = s.sortedSquares(arr);

		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}
