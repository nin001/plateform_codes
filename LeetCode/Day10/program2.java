// Longest comman prefix in Array


class Solution {
    public String longestCommonPrefix(String[] strs) {
        int min = Integer.MAX_VALUE;
        for(int i = 0 ; i<strs.length ; i++) {
            if(min>strs[i].length()) {
                min = strs[i].length();
            }
        }

        StringBuilder str = new StringBuilder();

        char first[] = strs[0].toCharArray();


        for(int i = 0 ; i<first.length ; i++) {
            int flag = 0;
            if(i+1 > min)
                break;
            for(int j = 0 ; j<strs.length ; j++) {
                if(first[i] == strs[j].charAt(i))
                    flag = 1;
                else {
                    flag = 0;
                    break;
                }
            }

            if(flag == 1) {
                str.append(first[i]);
            }else {
                break;
            }
        }

        return str.toString();
    }
}

class Client {
	public static void main(String args[]) {

		String strs[] = new String[]{"dog","racecar","car"};

		Solution s = new Solution();

		System.out.println(s.longestCommonPrefix(strs));
	}
}
