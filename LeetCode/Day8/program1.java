
import java.util.*;

class Solution {
    public void merge(int[] arr1, int m, int[] arr2, int n) {
	    int i = 0;
	    int j = 0;

	    while(i<m && j<n) {
		    if(arr1[i]<=arr2[j]) {
			    i++;
		    }else {
			    int temp = arr1[i];
			    arr1[i] = arr2[j];
			    arr2[j] = temp;

			    Arrays.sort(arr2);
		    }
	    }

	    while(j<n) {
		    arr1[i++] = arr2[j++];
	    }
    }
}

class Client {
	public static void main(String args[]) {

		int arr1[] = new int[]{5,6,7,0,0,0};

		int arr2[] = new int[]{1,2,3};

		Solution s = new Solution();

		s.merge(arr1,3,arr2,3);

		for(int i = 0 ; i<arr1.length ; i++) {
			System.out.print(arr1[i] + " ");
		}
		System.out.println();
	}
}
