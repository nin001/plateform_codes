// Leetcode - 628

// maximum product of Three numbers

class Solution {
    /*public int maximumProduct(int[] arr) {
	    int index1 = -1;
	    int max = Integer.MIN_VALUE;
	    for(int i = 0 ; i<arr.length ; i++) {
		    if(arr[i]>max) {
			    max = arr[i];
			    index1 = i;
		    }
	    }

	    int index2 = -1;
	    int secMax = Integer.MIN_VALUE;
	    for(int i = 0 ; i<arr.length ; i++) {
		    if(secMax<arr[i] && arr[i]<=max && i!=index1) {
			    secMax = arr[i];
			    index2 = i;
		    }
	    }

	    int thirdMax = Integer.MIN_VALUE;
	    for(int i = 0 ; i<arr.length ; i++) {
		    if(thirdMax<arr[i] && arr[i]<=secMax && i!=index1 && i!=index2) {
			    thirdMax = arr[i];
		    }
	    }

	 
	    return max*secMax*thirdMax;
    }*/

	public int maximumProduct(int[] arr) {
        int index1 = -1;
        int max = Integer.MIN_VALUE;
        int negIndex = -1;
        int negativeMax = Integer.MAX_VALUE;
        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i]>max) {
                index1 = i;
                max = arr[i];
            }
            if(arr[i]<0) {
                if(arr[i]<negativeMax) {
                    negIndex = i;
                    negativeMax = arr[i];
                }
            }
        }

        int index2 = -1;
        int secMax = Integer.MIN_VALUE;
        int secNegativeMax = Integer.MAX_VALUE;
        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i]>secMax && arr[i]<=max && i!=index1) {
                index2 = i;
                secMax = arr[i];
            }
            if(arr[i]<0) {
                if(arr[i]<secNegativeMax && arr[i]>=negativeMax && i!=negIndex) {
                    secNegativeMax = arr[i];
                }
            }
        }

        int thirdMax = Integer.MIN_VALUE;
        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i]>thirdMax && arr[i]<=secMax && i!=index1 && i!=index2)
                thirdMax = arr[i];
        }

        if(negIndex == -1) {
            return max*secMax*thirdMax;
        }else {
		System.out.println(negativeMax + " " + secNegativeMax + " " + thirdMax);
		System.out.println(max + " " + secMax + " " + thirdMax);
            if(Math.abs(negativeMax)>=max && Math.abs(secNegativeMax)>=secMax) {
		   return Math.max(Math.max(negativeMax*secNegativeMax*thirdMax,negativeMax*secNegativeMax*secMax),negativeMax*secNegativeMax*max);
            }
        }
	System.out.println("..");
        return max*secMax*thirdMax;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{-1,-2,-3};

		Solution s = new Solution();

		System.out.println(s.maximumProduct(arr));
	}
}
