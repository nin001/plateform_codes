class Solution {
    public int longestConsecutive(int[] arr) {
        /**First approch that comes in my mind was to sort the array
        and then iterate through the array and check for longest subsequence
         */

        if(arr.length == 0) return 0;
        Arrays.sort(arr);

        // Array will be sorted and then we loop the array and find the 
        // longest subsequence if in sequence then arr[i] will be equal to arr[i+1]-1
        // This logic is used here , 
        // And if found the sequence is breaked , we just increment the 
        // count as the arr[i] value also includes in the sequence
        int maxCount = Integer.MIN_VALUE;
        int count = 1; // Start count at 1 since at least one element is always considered part of the subsequence
        for (int i = 0; i < arr.length - 1; i++) {
            if(arr[i] + 1 == arr[i + 1] ) {
                count++;
            }else if(arr[i] == arr[i+1]) {
                continue;
            }else{
                maxCount = Math.max(maxCount,count);
                count = 1;
            }
        }
        maxCount = Math.max(maxCount, count);
        return maxCount;
    }
}
