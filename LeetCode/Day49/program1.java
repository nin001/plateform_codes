class Solution {
    public int largestRectangleArea(int[] heights) {

        // The approch i came up with was for each element calculating the 
        // smaller element on its left and smaller element on its right 
        // after that smaller Element Index+1 / -1 on both sides(left and right)
        // and then multiplying by the height of the element
        // Time complexity of this is n^2
        
        // Approch for this particular problem, i refered from a video is 
        // calculating the leftSmall (boundary) and rightSmall(boundary) for 
        // each element
        // and using the formula -> (rightSmaller - leftSmaller + 1) * arr[i]

        int maxArea = Integer.MIN_VALUE;
        int leftSmall[] = new int[heights.length];
        int rightSmall[] = new int[heights.length];
        Stack<Integer> stack = new Stack<Integer>();

        for(int i = 0 ; i<heights.length ; i++) {
            
            while(!stack.isEmpty() && heights[stack.peek()]>=heights[i]) {
                stack.pop();
            }

            if(stack.isEmpty()) 
                leftSmall[i] = 0;
            else
                leftSmall[i] = stack.peek()+1;
            stack.push(i);
        }

        while(!stack.isEmpty()) {
            stack.pop();
        }

        for(int i = heights.length-1 ; i>=0 ; i--) {
            while(!stack.isEmpty() && heights[stack.peek()] >= heights[i]) {
                stack.pop();
            }

            if(stack.isEmpty()) {
                rightSmall[i] = heights.length-1;
            }else {
                rightSmall[i] = stack.peek()-1;
            }
            stack.push(i);
        }

        for(int i = 0 ; i<heights.length ; i++) {
            if(maxArea < ((rightSmall[i] - leftSmall[i] + 1) * heights[i])) {
                maxArea = (rightSmall[i] - leftSmall[i] + 1) * heights[i];
            }
        }

        return maxArea;
    }
}
