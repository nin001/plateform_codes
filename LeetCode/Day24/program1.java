// Maximum subarray leetcode - 53

class Solution {
    public int maxSubArray(int[] arr) {

        int currentSum = arr[0];
        int maxSum = arr[0];
        int sum = 0;

        for(int i = 1 ; i<arr.length ; i++) {
            sum = currentSum+arr[i];
            if(sum>arr[i]) {
                currentSum = sum;
            }else {
                currentSum = arr[i];
            }

            if(maxSum<currentSum) {
                maxSum = currentSum;
            }
        }

        return maxSum;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{1};

		Solution s = new Solution();

		System.out.println(s.maxSubArray(arr));
	}
}
