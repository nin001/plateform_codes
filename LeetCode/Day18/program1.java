// Leetcode 75
// Sort colors

/*
class Solution {
    public void sortColors(int[] arr) {
        int count0 = 0;
        int count1 = 0;

        for(int i = 0 ; i<arr.length ; i++) {
            if(arr[i] == 0) {
                count0++;
            }else if(arr[i] == 1) {
                count1++;
            }
        }

        for(int i = 0 ; i<arr.length ; i++) {
            if(count0>0) {
                arr[i] = 0;
                count0--;
            }else if(count1>0) {
                arr[i] = 1;
                count1--;
            }else {
                arr[i] = 2;
            }
        }
    }
}
*/

class Solution {
	public void sortColors(int arr[]) {
		//three pointer approch

        int i = 0;      //for0
        int j = 0;      //for1
        int k = arr.length-1;

        while(j<k) {
            if(arr[j] == 0) {
                int temp = arr[j];
                arr[j] = arr[i];
                arr[i] = temp;
                i++;
                j++;
            }else if(arr[j] == 1) {
                j++;
            }else {
                int temp = arr[j];
                arr[j] = arr[k];
                arr[k] = temp;
                j++;
                k--;
            }
        }
	}
}
class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{2,0,2,1,1,0};

		new Solution().sortColors(arr);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
