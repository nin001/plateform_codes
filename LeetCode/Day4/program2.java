// Next Greater Element
/*
The next greater element of some element x in an array is the first greater element that is to the right
of x in the same array.

You are given two distinct 0-indexed integer arrays nums1 and nums2, where nums1 is a subset of nums2.

For each 0 <= i < nums1.length, find the index j such that nums1[i] == nums2[j] and determine the 
next greater element of nums2[j] in nums2. If there is no next greater element, then the answer for 
this query is -1.

Return an array ans of length nums1.length such that ans[i] is the next greater element as described 
above.*/

import java.util.*;

class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {

	    int ans[] = new int[arr1.length];
        for(int i = 0 ; i<arr1.length ; i++) {
            int flag = 0;
            for(int j = 0 ; j<arr2.length ; j++) {
                if(arr2[j]==arr1[i]) {
                    for(int k = j+1 ; k<arr2.length ; k++) {
                        if(arr2[k]>arr1[i]) {
                            flag = 1;
                            ans[i] = arr2[k];
                            break;
                        }
                    }
                    break;
                }
            }
            if(flag == 0)
                ans[i] = -1;
        }
        return ans;
    }
}

class Client {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter n1 :");
		int n1 = sc.nextInt();

		int arr1[] = new int[n1];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr1.length ; i++) {
			arr1[i] = sc.nextInt();
		}
		
		System.out.println("Enter n2 :");
		int n2 = sc.nextInt();

		int arr2[] = new int[n2];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr2.length ; i++) {
			arr2[i] = sc.nextInt();
		}

		Solution s = new Solution();

		int ret[] = s.nextGreaterElement(arr1,arr2);
	}
}
