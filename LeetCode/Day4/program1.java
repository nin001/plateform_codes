// Leetcode 485
// Given a binary array nums, return the maximum number of consecutive 1's in the array.
import java.util.*;

class Solution {
    public int findMaxConsecutiveOnes(int[] arr) {

	    int start = 0;
	    int end = 0;

	    int maxOnes = 0;
	    int consecMax = -1;
	    while(end<arr.length && start<=end) {
		    if(arr[end] == 1) {
			    end++;
			    maxOnes++;
		    }else {
			    if(consecMax<maxOnes) {
				    consecMax = maxOnes;
			    }
			    maxOnes = 0;
			    start = end+1;
			    end = start;
		    }
	    }

	    if(consecMax<maxOnes) 
		   return maxOnes; 
	    return consecMax;
    }
}

class Client {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<size ; i++) {
			arr[i] = sc.nextInt();
		}

		Solution s = new Solution();

		System.out.println("Max Consecutive Ones : "+ s.findMaxConsecutiveOnes(arr));
	}
}
