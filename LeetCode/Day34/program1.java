import java.util.List;
import java.util.ArrayList;

class Solution {
    public List<Integer> spiralOrder(int[][] matrix) {
        
        List<Integer> retList = new ArrayList<Integer>();
        
        int m = matrix.length;
        int n = matrix[0].length;
        
        // flag to handel the condition when rowLeft is adding 
        // and it is not its first addition
        boolean rowRightflag = false;

        // flags to print
        boolean rowRight = true;
        boolean rowLeft = false , colUp = false , colDown = false;

        // variables that increments , so that visited rows and cols 
        // could be skipped
        int a = 0 , b = 0 , c = 0 , d = 1;

        // iterator variables
        int i = 0 , j = 0;
        while(retList.size() < m*n) {
            if(rowRight) {
                // print logic
                if(rowRightflag) {
                    j++;
                }
                while(j<n-a) {
                    retList.add(matrix[i][j]);
                    j++;
                }
                a++;
                j--;    // prev condition false kela sathi invalid index jhalela tela resolve kela
                // change the flags 
                rowRight = false;
                colDown = true;
            }else if(colDown) {
                i++;
                while(i<m-b) {
                    retList.add(matrix[i][j]);
                    i++;
                }
                b++;
                i--; // invalid index resolve
                //-------
                colDown = false;
                rowLeft = true;
            }else if(rowLeft){
                
                j--;
                while(j >= 0+c) {
                    retList.add(matrix[i][j]);
                    j--;
                }
                c++;
                j++; // resolve illegal index
                //------
                rowLeft = false;
                colUp = true;
            }else {
                
                i--;
                while(i >= 0+d) {
                    retList.add(matrix[i][j]);
                    i--;
                }
                d++;
                i++;        // invalid index resolve
                // -----
                colUp = false;
                rowRight = true;
                rowRightflag = true;
                
            }
            
        }
        return retList;
    }
}
