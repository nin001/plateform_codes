class Solution {
    public double findMedianSortedArrays(int[] arr1, int[] arr2) {
        // Idea to find the median of Two Sorted array is by 
        // First calculating the size and if even median will be n/2-a + n/2 divided by 2
        // and if the size is odd median will be n/2

        // First approch here is to create a array list and insert the element upto the n/2 size and
        // return the element (last) if even return last and second last / 2

        // List<Integer> medianList = new ArrayList<Integer>();

        // int i = 0;  // pointer to the first array
        // int j = 0;  // pointer to the second array

        // int size = arr1.length + arr2.length;

        // while(medianList.size() < (size/2)+1 && i<arr1.length && j<arr2.length) {        // fill the elements untill the size/2 is satisfied
        //     if(arr1[i]<arr2[j]) {
        //         medianList.add(arr1[i]);        // adds arr1 element, we want sorting
        //         i++;
        //     }else {
        //         medianList.add(arr2[j]);    // adds arr2 if arr2 element is shorter than the 
        //         j++;                        // arr1 element
        //     }
        // }

        // while(i<arr1.length && medianList.size() < (size/2)+1) {
        //     medianList.add(arr1[i++]);
        // }

        // while(j<arr2.length && medianList.size() < (size/2)+1) {
        //     medianList.add(arr2[j++]);
        // }

        // double median = 0;
        // if(size%2 == 0) {   // if even size calculate last and second last divided by 2 and return the median
        //     median = medianList.get(medianList.size()-1)+medianList.get(medianList.size()-2);
        //     return median/2;
        // }

        // median = medianList.get(size/2);    // if odd then simply return the middle element
        // return median;       // SUBMITTED WITH 2MS and Memory 46.02

        // Second approch is to not use array and just iterate through the array and find the indexes
        // we will calculate the median through the iterator rather than arrayList


        int i = 0;  // pointer to the first array
        int j = 0;  // pointer to the second array

        int size = 0;
        int lastAdded = -1;     // lastAdded variable to check which element added
        while(size < ((arr1.length+arr2.length)/2)+1 && i<arr1.length && j<arr2.length) {        // fill the elements untill the size/2 is satisfied
            if(arr1[i]<arr2[j]) {
                //medianList.add(arr1[i]);        // adds arr1 element, we want sorting
                i++;
                lastAdded = 1;
            }else {
                //medianList.add(arr2[j]);    // adds arr2 if arr2 element is shorter than the 
                j++;                        // arr1 element
                lastAdded = 2;
            }
            size++;
        }

        while(i<arr1.length && size < ((arr1.length+arr2.length)/2)+1) {
            i++;
            size++;
            lastAdded = 1;
        }

        while(j<arr2.length && size < ((arr1.length+arr2.length)/2)+1) {
            j++;
            size++;
            lastAdded = 2;
        }

        double median = 0;
        // Test cases for this iterator approch is that checking if i || j == 0
        // if any one of them is equal to 0 that means the median is withing the other array

        if((arr1.length+arr2.length)%2 == 0) {
            if(i == 0) {
                median = arr2[j-1]+arr2[j-2];
            }else if(j == 0) {
                median = arr1[i-1]+arr1[i-2];
            }else {
                median = arr1[i-1]+arr2[j-1];
            }
            return median/2;
        }

        if(i == 0) {
            median = arr2[j-1];
        }else if(j == 0) {
            median = arr1[i-1];
        }else if(lastAdded == 1){
            median = arr1[i-1];
        }else {
            median = arr2[j-1];
        }

        return median;
        
    }
}
