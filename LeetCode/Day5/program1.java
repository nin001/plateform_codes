// pivot index

class Solution {
	int pivotIndex(int arr[]) {
		int sumArr[] = new int[arr.length];

		sumArr[0] = arr[0];

		for(int i = 1 ; i<arr.length ; i++) {
			sumArr[i] = sumArr[i-1] + arr[i];
		}

		for(int i = 0 ; i<arr.length ; i++) {
			if(i == 0) {
				if(sumArr[arr.length-1]-sumArr[i] == 0) {
					return i;
				}
			}else if(i == arr.length-1) {
				if(sumArr[i-1] == 0) {
					return i;
				}
			}else {
				int left = sumArr[i-1];
				int right = sumArr[arr.length-1] - sumArr[i];
				if(left == right)
					return i;
			}
		}
		return -1;
	}
}
