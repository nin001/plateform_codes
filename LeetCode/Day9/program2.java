// Leetcode - 2460
// Apply operations to an array

class Solution {
    public int[] applyOperations(int[] arr) {
        int i = 0;
        int j = 1;

        while(i<arr.length && j<arr.length) {
            if(arr[i] == arr[j]) {
                arr[i] *= 2;
                arr[j] = 0;
                i += 2;
                j += 2;
            }else {
            	i++;
            	j++;
	    }
        }

        i = 0;
        while(i<arr.length) {
            if(arr[i] == 0) {
                j = i+1;
                while(j<arr.length) {
                    if(arr[j] != 0) {
                        int temp = arr[j];
                        arr[j] = arr[i];
                        arr[i] = temp;
                        break;
                    }
                    j++;
                }
                if(j == arr.length)
                    return arr;
            }
            i++;

        }

        return arr;
    }
}

class Client {
	public static void main(String args[]) {
		int arr[] = new int[]{0,1};

		Solution s = new Solution();

		int ret[] = s.applyOperations(arr);

		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i]);
		}
		System.out.println();
	}
}
