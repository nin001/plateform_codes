// Missing number in arrray
import java.util.*;

class Solution {
    public int missingNumber(int[] arr) {
	    /*Set<Integer> set = new HashSet<Integer>();

	    for(int i = 0 ; i<arr.length ; i++) {
		    set.add(arr[i]);
	    }

	    for(int i = 0 ; i<=arr.length ; i++) {
		    if(!set.contains(i)) {
			    return i;
		    }
	    }
	    return -1;*/

	    int sum = (arr.length*(arr.length+1))/2;

	    for(int i = 0 ; i<arr.length ; i++) {
		    sum -= arr[i];
	    }
	    return sum;
    }
}

class Client {
	public static void main(String args[]) {

		int arr[] = new int[]{9,6,4,2,3,5,7,0,1};

		Solution sol = new Solution();
		System.out.println(sol.missingNumber(arr));
	}
}
