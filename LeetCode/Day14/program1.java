import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

class Solution {
    public int[] intersection(int[] arr1, int[] arr2) {
        int max = Integer.MIN_VALUE;

        for(int i = 0 ; i<arr1.length ; i++) {
            if(arr1[i]>max) {
                max = arr1[i];
            }
        }

        int flagArr[] = new int[max+1];

        for(int i = 0 ; i<arr1.length ; i++) {
            flagArr[arr1[i]] = 1;
        }

        Set<Integer> set = new HashSet<Integer>();

        for(int i = 0 ; i<arr2.length ; i++) {
            if(arr2[i]<flagArr.length && flagArr[arr2[i]] == 1) {
                set.add(arr2[i]);
            }
        }

        int ret[] = new int[set.size()];

        Iterator itr = set.iterator();
        int i = 0;
        while(itr.hasNext()) {
            ret[i++] = (int)itr.next();
        }

        return ret;
    }
}
class Client {
	public static void main(String args[]) {

		int arr1[] = new int[]{4,9,5};
		int arr2[] = new int[]{9,4,9,8,4};

		Solution s = new Solution();

		int intersection[] = s.intersection(arr1,arr2);

		for(int i = 0 ; i<intersection.length ; i++) {
			System.out.print(intersection[i] + " ");
		}
		System.out.println();
	}
}
