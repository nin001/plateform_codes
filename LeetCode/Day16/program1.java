/*
TLE 
class Solution {
    public int maxProfit(int[] prices) {
        
        int profit = Integer.MIN_VALUE;
        for(int i = 0 ; i<prices.length ; i++) {
            for(int j = i+1 ; j<prices.length ; j++) {
                if(profit<prices[j]-prices[i]) {
                    profit = prices[j]-prices[i];
                }
            }
        }
        
        return (profit<0)?0:profit;
    }
}*/

class Solution {
    public int maxProfit(int[] prices) {

        int buy = prices[0];
        int profit = 0;

        for(int i = 1 ; i<prices.length ; i++) {
            if(prices[i]>buy) {
                profit = Math.max(profit,prices[i]-buy);
            }else {
                buy = prices[i];
            }
        }

        return profit;
    }
}
